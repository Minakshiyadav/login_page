from django.contrib.auth.models import User
from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

class SignUpForm(UserCreationForm):
 password2 = forms.CharField(label='Confirm Password (again)', widget=forms.PasswordInput)
 class Meta:
  model = User
  fields = ['username', 'first_name', 'last_name', 'email']
  labels = {'first_name':'Name','email': 'Email','password':'Password'}

class EditUserProfileForm(UserChangeForm):
 password = None
 class Meta:
  model = User
  fields = ['first_name', 'email','password']
  labels = {'first_name':'Name','email': 'Email','password':'Password'}
